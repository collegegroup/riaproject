import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import {RouterModule} from '@angular/router';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { InicioComponent } from './components/inicio/inicio.component';
import { BusquedaComponent } from './components/busqueda/busqueda.component';
import { DetalleComponent } from './components/detalle/detalle.component';
import { PopularesComponent } from './components/populares/populares.component';
import { ValoradasComponent } from './components/valoradas/valoradas.component';
import { ProximamenteComponent } from './components/proximamente/proximamente.component';
import { SeriesPopularComponent } from './components/seriespopular/seriespopular.component';
import { EnvivoComponent } from './components/envivo/envivo.component';
import { PersonaspopularesComponent } from './components/personaspopulares/personaspopulares.component';
// import { BusquedaPaisComponent } from './components/filtrosUsuario/busqueda-pais/busqueda-pais.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    NotFoundComponent,
    FooterComponent,
    InicioComponent,
    BusquedaComponent,
    DetalleComponent,
    PopularesComponent,
    ValoradasComponent,
    ProximamenteComponent,
    SeriesPopularComponent,
    EnvivoComponent,
    PersonaspopularesComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    MatDialogModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    RouterModule.forRoot([
      { path: 'inicio', component: InicioComponent },
      { path: 'populares', component:PopularesComponent },
      { path: 'masValoradas', component:ValoradasComponent},
      { path: 'proximamente', component:ProximamenteComponent},
      { path: '', component: InicioComponent },
      { path: 'busqueda/:consulta', component: BusquedaComponent },
      { path: 'detalle/:id/:type', component: DetalleComponent } ,
      { path: 'seriespopular', component: SeriesPopularComponent },
      { path: 'envivo', component: EnvivoComponent },
      { path: 'personaspopulares', component: PersonaspopularesComponent }
      
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
