import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PeliculasdbService {
  //metemos unos atributos aca
  private apiKey = 'ec62ef974fee817b60f9c3e9a82fc986';
  private apiAddress = 'https://api.themoviedb.org/3';
  private results;

  constructor(private http: HttpClient) { }

  //metodos de las queries, hay parametros hardcodeados para que salga en ingles
  getPopular():Observable<any> {
    let url = this.apiAddress + '/movie/popular?api_key=' + this.apiKey + '&language=en-US&page=1';
    return this.http.get<any>(url, {responseType: "json"});
  }


  getSeriesPopular():Observable<any> {
    let url = this.apiAddress + '/tv/popular?api_key=' + this.apiKey + '&language=en-US&page=1';
    return this.http.get<any>(url, {responseType: "json"});
  }

  getPersonasPopular():Observable<any> {
    let url = this.apiAddress + '/person/popular?api_key=' + this.apiKey + '&language=en-US&page=1';
    return this.http.get<any>(url, {responseType: "json"});
  }

  getSeriesEnVivo():Observable<any> {
    let url = this.apiAddress + '/tv/on_the_air?api_key=' + this.apiKey + '&language=en-US&page=1';
    return this.http.get<any>(url, {responseType: "json"});
  }

  getTopPeliculas():Observable<any> {
    let url = this.apiAddress + '/movie/top_rated?api_key=' + this.apiKey + '&language=en-US&page=1';
    return this.http.get<any>(url, {responseType: "json"});
  }

  getSiguientes():Observable<any> {
    let url = this.apiAddress + '/movie/upcoming?api_key=' + this.apiKey + '&language=en-US';
    return this.http.get<any>(url, {responseType: "json"});
  }

  //hay que mirar bien este metodo, como pasar el parametro correctamente
  getDetalles(peli_id: number, type: String): Observable<any> {
    let foo = peli_id.toString();
    let url = '';
    if (type == 'movie'){
      url = this.apiAddress + '/movie/' + foo + '?api_key=' + this.apiKey + '&language=en-US';
    } 
    else if (type == 'tv'){
      url = this.apiAddress + '/tv/' + foo + '?api_key=' + this.apiKey + '&language=en-US';
    }
    else {
      url = this.apiAddress + '/person/' + foo + '?api_key=' + this.apiKey + '&language=en-US';
    }
    return this.http.get<any>(url, {responseType: "json"});
  }

  //metodo para la imagen
  getImagen(path: String): String {
    let url = 'https://image.tmdb.org/t/p/w154';
    url += path;
    return url;
  }

  getImagenGrande(path: String): String {
    let url = 'https://image.tmdb.org/t/p/w300';
    url += path;
    return url;
  }

  //metodo para buscar
  getBusqueda(path: String):Observable<any> {
    let url = this.apiAddress + '/search/multi/?api_key=' +  this.apiKey + '&language=en-US&query=' + path + '&page=1';
    return this.http.get<any>(url, {responseType: "json"});
  }

}
