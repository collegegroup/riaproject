import { TestBed } from '@angular/core/testing';

import { PeliculasdbService } from './peliculasdb.service';

describe('PeliculasdbService', () => {
  let service: PeliculasdbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PeliculasdbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
