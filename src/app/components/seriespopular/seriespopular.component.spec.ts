import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeriesPopularComponent } from './seriespopular.component';

describe('SeriesPopularComponent', () => {
  let component: SeriesPopularComponent;
  let fixture: ComponentFixture<SeriesPopularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeriesPopularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeriesPopularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
