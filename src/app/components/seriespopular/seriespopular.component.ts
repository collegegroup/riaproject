import { Component, OnInit } from '@angular/core';
import { PeliculasdbService } from 'src/app/services/peliculasdb.service';

@Component({
  selector: 'app-seriespopular',
  templateUrl: './seriespopular.component.html',
  styleUrls: ['./seriespopular.component.css']
})




export class SeriesPopularComponent implements OnInit {
  seriesPopulares: Object;
  constructor(public peliApi: PeliculasdbService) { }

  ngOnInit() {
    this.peliApi.getSeriesPopular().subscribe(data => {this.seriesPopulares= data.results});
  }
}
