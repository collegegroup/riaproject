import { Component, OnInit } from '@angular/core';
import { PeliculasdbService } from 'src/app/services/peliculasdb.service';

@Component({
  selector: 'app-proximamente',
  templateUrl: './proximamente.component.html',
  styleUrls: ['./proximamente.component.css']
})




export class ProximamenteComponent implements OnInit {
  proximasPeliculas: Object;
  constructor(public peliApi: PeliculasdbService) { }

  ngOnInit() {
    this.peliApi.getSiguientes().subscribe(data => {this.proximasPeliculas= data.results});
  }
}