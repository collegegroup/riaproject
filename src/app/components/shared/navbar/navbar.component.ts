import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user = {
    usuario: '',
    password: '',
  };

  query = {
    consulta: ''
  }

  public formLogin: FormGroup;
  usuario = new FormControl('');
  password = new FormControl('');
  
  public formBusqueda: FormGroup;
  consulta = new FormControl('');

  constructor(private formBuilder: FormBuilder,
    private router: Router) { }
  
  ngOnInit() {

    this.armarFormulario();
    this.armarBusqueda();
  }

  private armarBusqueda(){
    this.formBusqueda = this.formBuilder.group({
      consulta: [""],
    });
  }

  private armarFormulario(){
    this.formLogin = this.formBuilder.group({
      usuario: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  public IniciarSesion(formValue) {
    let error = false;
    let msg = 'Debe Ingresar: \n';
    if (formValue.value.usuario === '') {
      msg += 'Usuario \n';
      error = true;
    }
    if (formValue.value.password === '') {
      msg += 'Contraseña \n';
      error = true;
    }
    if (error) {
      alert(msg);
    } else {
        if (formValue.value.usuario == 'admin' && formValue.value.password == 'admin') {
          this.user.usuario = formValue.value.usuario;
          this.user.password = formValue.value.password;
        }else if (formValue.value.usuario == 'user' && formValue.value.password == 'user'){
          this.user.usuario = formValue.value.usuario;
          this.user.password = formValue.value.password;
        } else {
          alert('Usuario o Contraseña incorrectos.');
        }
    };
  }

  public busqueda(form) {
    console.log(form.value);
    let valor = form.value.consulta;
    console.log(valor);
    this.router.navigate(['busqueda', valor]);
  }  
}