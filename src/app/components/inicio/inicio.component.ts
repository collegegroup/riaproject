import { Component, OnInit } from '@angular/core';
import { PeliculasdbService } from 'src/app/services/peliculasdb.service';


 @Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  peliculasTrend: Object;
  peliculasMejores: Object;
  peliculasSiguientes: Object;
  constructor(public peliApi: PeliculasdbService) { 
  }

  ngOnInit() {
    this.peliApi.getPopular().subscribe(data => {this.peliculasTrend = data.results});
    this.peliApi.getTopPeliculas().subscribe(data => {this.peliculasMejores = data.results});
    this.peliApi.getSiguientes().subscribe(data => {this.peliculasSiguientes = data.results});
  }
}