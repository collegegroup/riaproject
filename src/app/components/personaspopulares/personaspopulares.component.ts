import { Component, OnInit } from '@angular/core';
import { PeliculasdbService } from 'src/app/services/peliculasdb.service';

@Component({
  selector: 'app-personaspopulares',
  templateUrl: './personaspopulares.component.html',
  styleUrls: ['./personaspopulares.component.css']
})




export class PersonaspopularesComponent implements OnInit {
  personasPopulares: Object;
  constructor(public peliApi: PeliculasdbService) { }

  ngOnInit() {
    this.peliApi.getPersonasPopular().subscribe(data => {this.personasPopulares= data.results});
  }
}
