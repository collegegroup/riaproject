import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonaspopularesComponent } from './personaspopulares.component';

describe('PersonaspopularesComponent', () => {
  let component: PersonaspopularesComponent;
  let fixture: ComponentFixture<PersonaspopularesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonaspopularesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonaspopularesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
