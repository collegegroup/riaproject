import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { PeliculasdbService } from 'src/app/services/peliculasdb.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent implements OnInit {
  query: String;
  resultado: any;
  private sub: any;
  constructor(private route: ActivatedRoute,
    public peliApi: PeliculasdbService,
    private router: Router) {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

  ngOnInit(): void {
    this.query = this.route.snapshot.paramMap.get('consulta');
    this.peliApi.getBusqueda(this.query).subscribe(data => {this.resultado = data.results});
  }

}
