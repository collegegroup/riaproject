import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValoradasComponent } from './mas-valoradas.component';

describe('MasValoradasComponent', () => {
  let component: ValoradasComponent;
  let fixture: ComponentFixture<ValoradasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValoradasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValoradasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
