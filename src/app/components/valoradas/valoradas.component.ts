import { Component, OnInit } from '@angular/core';
import { PeliculasdbService } from 'src/app/services/peliculasdb.service';

@Component({
  selector: 'app-valoradas',
  templateUrl: './valoradas.component.html',
  styleUrls: ['./valoradas.component.css']
})

export class ValoradasComponent implements OnInit {
  masValoradas: Object;
  constructor(public peliApi: PeliculasdbService) { }

  ngOnInit() {
    this.peliApi.getTopPeliculas().subscribe(data => {this.masValoradas= data.results});
  }
}
