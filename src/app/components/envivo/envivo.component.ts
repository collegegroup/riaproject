import { Component, OnInit } from '@angular/core';
import { PeliculasdbService } from 'src/app/services/peliculasdb.service';

@Component({
  selector: 'app-envivo',
  templateUrl: './envivo.component.html',
  styleUrls: ['./envivo.component.css']
})




export class EnvivoComponent implements OnInit {
  seriesPopulares: Object;
  constructor(public peliApi: PeliculasdbService) { }

  ngOnInit() {
    this.peliApi.getSeriesEnVivo().subscribe(data => {this.seriesPopulares= data.results});
  }
}