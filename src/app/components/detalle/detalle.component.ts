import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { PeliculasdbService } from 'src/app/services/peliculasdb.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {

  itemId: number;
  type: string;
  dummy: any;
  private sub: any;
  constructor(private route: ActivatedRoute,
    public peliApi: PeliculasdbService) {}

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {this.itemId = params['id']; this.type = params['type']});
    this.sub = this.peliApi.getDetalles(this.itemId, this.type).subscribe(data => {this.dummy = data});
  }

}
