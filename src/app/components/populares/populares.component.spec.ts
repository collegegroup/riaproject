import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { popularesComponent } from './populares.component';

describe('PopularesComponent', () => {
  let component: popularesComponent;
  let fixture: ComponentFixture<popularesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ popularesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(popularesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
