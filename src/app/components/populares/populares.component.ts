import { Component, OnInit } from '@angular/core';
import { PeliculasdbService } from 'src/app/services/peliculasdb.service';

@Component({
  selector: 'app-populares',
  templateUrl: './populares.component.html',
  styleUrls: ['./populares.component.css']
})




export class PopularesComponent implements OnInit {
  peliculasPopulares: Object;
  constructor(public peliApi: PeliculasdbService) { }

  ngOnInit() {
    this.peliApi.getPopular().subscribe(data => {this.peliculasPopulares= data.results});
  }
}